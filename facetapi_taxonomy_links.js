Drupal.behaviors.facetapi_taxonomy_links = function(context) {

  // The following is copied from facetapi.js, for adding checkboxes.
  var settings = Drupal.settings;
  // Iterates over facet settings, applies functionality like the "Show more"
  // links for block realm facets.
  // @todo We need some sort of JS API so we don't have to make decisions
  // based on the realm.
  if (settings.facetapi) {
    for (var index in settings.facetapi.facets) {
      if (null != settings.facetapi.facets[index].makeCheckboxes) {
        // Find all checkbox facet links and give them a checkbox.
        // Drupal 6 problem, it did not want to select on the real ID
        $('.facetapi-facetapi_taxonomy_checkbox_links a.facetapi-checkbox', context).each(Drupal.facetapi.makeCheckbox);
      }
      if (null != settings.facetapi.facets[index].limit) {
        // Applies soft limit to the list.
        Drupal.facetapi.applyLimit(settings.facetapi.facets[index]);
      }
    }
  }
}
